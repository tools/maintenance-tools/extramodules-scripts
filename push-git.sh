#!/bin/bash

start_agent(){
    echo "Initializing SSH agent..."
    ssh-agent | sed 's/^echo/#echo/' > "$1"
    chmod 600 "$1"
    . "$1" > /dev/null
    ssh-add
}

ssh_add(){
    local ssh_env="/home/$(whoami)/.ssh/environment"

    if [ -f "${ssh_env}" ]; then
         . "${ssh_env}" > /dev/null
         ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
            start_agent ${ssh_env};
        }
    else
        start_agent ${ssh_env};
    fi
}

ssh_add

for pkg in $(ls */PKGBUILD | sed s'|/PKGBUILD||g') ; do
   if [ -e ${pkg}/.git ]; then
      cd ${pkg}
      echo "Push ${pkg}"
      git push
      cd ..
   fi
done
