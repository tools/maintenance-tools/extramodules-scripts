#!/bin/bash

start_agent(){
    msg2 "Initializing SSH agent..."
    ssh-agent | sed 's/^echo/#echo/' > "$1"
    chmod 600 "$1"
    . "$1" > /dev/null
    ssh-add
}

ssh_add(){
    local ssh_env="/home/$(whoami)/.ssh/environment"

    if [ -f "${ssh_env}" ]; then
         . "${ssh_env}" > /dev/null
         ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
            start_agent ${ssh_env};
        }
    else
        start_agent ${ssh_env};
    fi
}

ssh_add

path=$(pwd)
modules=${path##*/}

for pkg in $(ls */PKGBUILD | sed s'|/PKGBUILD||g') ; do
   if [ ! -e ${pkg}/.git ]; then
      cd ${pkg}
      echo "Create ${pkg} GIT"
      git init
      git remote add origin ssh://git@gitlab.manjaro.org:22277/packages/extra/$modules/$pkg.git
      git add .
      git commit -m "Initial commit"
      git push -u origin master
      popd &>/dev/null
      #sleep 10s
      cd ..
   fi
done
