#!/bin/bash

start_agent(){
    echo "Initializing SSH agent..."
    ssh-agent | sed 's/^echo/#echo/' > "$1"
    chmod 600 "$1"
    . "$1" > /dev/null
    ssh-add
}

ssh_add(){
    local ssh_env="/home/$(whoami)/.ssh/environment"

    if [ -f "${ssh_env}" ]; then
         . "${ssh_env}" > /dev/null
         ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
            start_agent ${ssh_env};
        }
    else
        start_agent ${ssh_env};
    fi
}

ssh_add

for pkg in $(ls */PKGBUILD | sed s'|/PKGBUILD||g') ; do
   if [ -e ${pkg}/.git ]; then
      cd ${pkg}
      pkgver=$(grep pkgver= PKGBUILD -m1 | cut -d= -f2)
      pkgrel=$(grep pkgrel= PKGBUILD -m1 | cut -d= -f2)
      echo "Update ${pkg}"
      git pull
      ! $(. ./PKGBUILD >/dev/null 2>&1) && sanity=fail
      if [[ $sanity == "fail" ]]; then
          printf "ERROR: Sanity-check for ${pkg}/PKGBUILD failed.\n"
          exit 1
      fi
      git commit -am "[pkg-upd] $pkgver-$pkgrel"
      git push
      cd ..
   fi
done
